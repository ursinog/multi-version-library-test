all: clean buildall main_hab.bin main_hab_with_static_foo.bin main_foo1.bin main_foo2.bin

main_hab.bin:
	gcc -c main.c -o main.o
	gcc main.o -Lhab -lhab -Lfoo1 -lfoo -o $@

main_hab_with_static_foo.bin:
	gcc -c main.c -o main.o
	gcc main.o -Lfoo1 -lfoo -Lhab -lhab_with_static_foo -o $@

main_foo1.bin:
	gcc -c main_foo.c -o main_foo.o
	gcc main_foo.o -Lfoo1 -lfoo -o $@

main_foo2.bin:
	gcc -c main_foo.c -o main_foo.o
	gcc main_foo.o -Lfoo2 -lfoo -o $@

clean:
	rm -rf *.o *.a *.so.*
	rm -rf *.bin

cleanall: clean
	cd foo1 && make clean
	cd foo2 && make clean
	cd hab && make clean

buildall:
	cd foo1 && make all
	cd foo2 && make all
	cd hab && make all

