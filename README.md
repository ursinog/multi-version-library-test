# How to using multiple version libraries on same application

I would have an application that depends to `libfoo.so.1` and `libhab` that
depends to `libfoo.so.2`.

```
libfoo.so.1 --> exports getFoo() function that waits 5sec
libfoo.so.2 --> exports speedly getFoo() function that no waits

libhab.so.1 --> exports getHab() function, that calls getFoo()
                this function is compliant and tested with libfoo.so.2
                and not with libfoo.so.1.

main_foo.c  --> call getFoo()

main.c      --> call getHab() and the getFoo()
                I want no wait on getHab(), because libhab uses
                libfoo2, but wait on getFoo() because main uses libfoo1
```

Please use `make` to test different linking options.

To run the application must be set the `LD_LIBRARY_PATH` with library.

Example:
```
export LD_LIBRARY_PATH=foo1:hab:$LD_LIBRARY_PATH
./main_foo1.bin
```

## Results

* `main_foo1.bin`: wait 5s
* `main_foo2.bin`: no wait
* `main_hab`: wait 5s + 5s
* `main_hab_with_static_foo`: same as before

The last one I thought that using static libfoo on libhab, it works.
But instead, when linking the main, it resolve the missing symbol getFoo
with the libfoo linked in main. So do not use the statically linked
function.
